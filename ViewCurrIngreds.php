<?php
 
// Get a connection for the database
require_once('mysqli_connect.php');

// Create a query for the database
$query = "SELECT derived.rn AS dr, SUM(derived.rq) AS totes
FROM (SELECT MenuItem.ItemID AS mii, MenuItem.name AS min, Requires.Quantity AS rq,
       Requires.name AS rn, Contains.ItemID as CII, Contains.quantity as cq
FROM MenuItem, Requires, Ingredient, Contains
WHERE MenuItem.ItemID = Requires.ItemID && Requires.name = Ingredient.name &&
	  Contains.ItemID = MenuItem.ItemID) derived
GROUP BY derived.rn";

// Get a response from the database by sending the connection
// and the query
$response = @mysqli_query($dbc, $query);

// If the query executed properly proceed
if($response){

echo '<table align="left"
cellspacing="5" cellpadding="8">

<tr><td align="left"><b>Ingredient Name</b></td>
<td align="left"><b>Total</b></td>';

// mysqli_fetch_array will return a row of data from the query
// until no further data is available
while($row = mysqli_fetch_array($response)){

echo '<tr><td align="left">' . 
$row['dr'] . '</td><td align="left">' . 
$row['totes'] . '</td><td align="left">';

echo '</tr>';
}

echo '</table>';

} else {

echo "Couldn't issue database query<br />";

echo mysqli_error($dbc);

}

// Close connection to the database
mysqli_close($dbc);

?>